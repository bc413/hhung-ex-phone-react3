import React, { Component } from "react";
import { data_phone } from "./Data";
import Detail from "./Detail";
import ListFone from "./ListFone";

export default class Ex_Phone extends Component {
  state = {
    dataFone: data_phone,
    phone: data_phone[0],
  };
  handleChangePhone = (phone) => {
    let arrphone = [...this.state.dataFone];
    let index = arrphone.findIndex((item) => {
      return item.maSP == phone.maSP;
    });
    if (index != -1) {
      this.setState({
        phone: data_phone[index],
      });
    }
  };
  render() {
    return (
      <div className="container">
        <h2>Ex_Phone</h2>
        <ListFone
          dataPhone={this.state.dataFone}
          handleChangePhone={this.handleChangePhone}
        />
        <Detail phone={this.state.phone} />
      </div>
    );
  }
}
