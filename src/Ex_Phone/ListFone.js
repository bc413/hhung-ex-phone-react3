import React, { Component } from "react";
import ItemPhone from "./ItemPhone";

export default class ListFone extends Component {
  renderPhone = () => {
    return this.props.dataPhone.map((item) => {
      return <ItemPhone item={item} click={this.props.handleChangePhone} />;
    });
  };
  render() {
    return <div className="row">{this.renderPhone()}</div>;
  }
}
