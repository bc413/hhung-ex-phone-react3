import React, { Component } from "react";

export default class Detail extends Component {
  render() {
    let {
      hinhAnh,
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
    } = this.props.phone;
    return (
      <div className="row">
        <div className="col-5">
          <img style={{ width: "100%" }} src={hinhAnh} alt="" />
        </div>
        <div className="col-7 mt-5 text-left">
          <p>
            <strong>Mã sản phẩm:</strong>
            {maSP}
          </p>
          <p>
            <strong>Tên Sản Phẩm: </strong> {tenSP}
          </p>
          <p>
            <strong>Màn Hình: </strong>
            {manHinh}
          </p>
          <p>
            <strong>Hệ Điều Hình: </strong>
            {heDieuHanh}
          </p>
          <p>
            <strong>Camera Trước: </strong>
            {cameraTruoc}
          </p>
          <p>
            <strong>Camera Sau: </strong>
            {cameraSau}
          </p>
          <p>
            <strong>Ram: </strong>
            {ram}
          </p>
          <p>
            <strong>Rom: </strong>
            {rom}
          </p>
          <p>
            <strong>Giá Bán: </strong>
            {giaBan}
          </p>
        </div>
      </div>
    );
  }
}
