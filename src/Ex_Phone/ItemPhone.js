import React, { Component } from "react";

export default class ItemPhone extends Component {
  render() {
    let { hinhAnh, tenSP, giaBan } = this.props.item;
    return (
      <div className="col-4">
        <div className="card border-primary h-100">
          <img className="card-img-top" src={hinhAnh} alt />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan}</p>
            <button
              onClick={() => {
                this.props.click(this.props.item);
              }}
              className="btn btn-primary"
            >
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}
